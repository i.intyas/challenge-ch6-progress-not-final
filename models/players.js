'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Players extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Players.hasOne(models.user_game_biodata, {
        foreignKey: 'UserId',
        onDelete : 'CASCADE',
        onUpdate : 'CASCADE',
        as:'user_game_biodata',
      })

      models.Players.hasOne(models.history, {
        foreignKey : 'userId',
        onUpdate : 'CASCADE',
        onDelete : 'CASCADE',
        as:'history'
      })
      
    }
  }
  Players.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Players',
    freezeTableName: true
  });
  return Players;
};