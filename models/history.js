'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.history.belongsTo(models.Players,{
        foreignKey : 'userId',
        onDelete : 'CASCADE',
        onUpdate : 'CASCADE',
        as:'history'
      })
    }
  }
  history.init({
    userId: DataTypes.STRING,
    score: DataTypes.STRING,
    lastPlayed: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'history',
    freezeTableName: true
  });
  return history;
};