'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.user_game_biodata.belongsTo(models.Players, {
        foreignKey : 'UserId',
        onDelete : 'CASCADE',
        onUpdate : 'CASCADE',
        as:'user_game_biodata'
      })
    }
  }
  user_game_biodata.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    UserId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_game_biodata',
    freezeTableName: true
  });
  return user_game_biodata;
};