const express = require("express");
const bodyParse = require("body-parser");
const path = require('path')
let posts = require("./data/users.json")
const { Sequelize, DataTypes } = require('sequelize');
const { request } = require("http");
const app = express();
const port = 3000;
app.use(express.urlencoded());
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'assets')));
app.use(bodyParse.json());
app.use(express.json())


const sequelize = new Sequelize('chapter_6', 'postgres', 'ryushikimura', {
    host: 'localhost',
    dialect: 'postgres',
    port: 8080,
});

async function testConnection() {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

const { Players, user_game_biodata, history } = require("./models");
const { send } = require("process");
const { render } = require("ejs");
testConnection();


app.get('/dashboard/list', async (req, res) => {
    const player = await Players.findAll({
        include: [
            {model: user_game_biodata, as:'user_game_biodata'},
        ]
    });
    res.render('dashboard/list.ejs', {
        player: player,
    });
});

app.get('/dashboard/biodata', async (req, res)=>{
    const player = await Players.findAll({
        include: [
            {model: user_game_biodata, as:'user_game_biodata'},
        ]
    });
    res.render('dashboard/biodata.ejs', {
        player : player,
    })
})

app.get('/dashboard/history', async (req, res)=>{
    const player = await Players.findAll({
        include: [
            {model: history, as:'history'},
        ]
    });
    res.render('dashboard/history.ejs', {
        player:player,
    })
})

app.post('/dashboard/create/submit', async (req, res) => {
    await Players.create(req.body);
    await user_game_biodata.create(req.body);
    await history.create(req.body);
    res.redirect('../list')
});


app.get('/dashboard/:id/delete', async (req, res) => {
    await history.destroy({
        where: {
            userId: req.params.id,
        },
    })
    await user_game_biodata.destroy({
        where :{
            UserId: req.params.id,
        }
    })
    await Players.destroy({
        where :{
            id: req.params.id,
        }
    })
    res.redirect("../list")
});

app.get('/dashboard/:id/edit', async (req, res) => {
    const player = await Players.findOne({
        where: {
            id: req.params.id,
        },
    })
    const player2 = await user_game_biodata.findOne({
        where: {
            UserId: req.params.id,
        },
    })
    const player3 = await history.findOne({
        where: {
            userId: req.params.id,
        },
    })
    res.render('dashboard/edit.ejs', { player: player, player2 : player2, player3 : player3 });
});

app.post('/dashboard/:id/edit/submit', async (req, res) => {
    await Players.update(req.body, { where: { id: req.params.id } })
    await user_game_biodata.update(req.body, { where: { UserId: req.params.id } })
    await history.update(req.body,{where: { userId : req.params.id}})
    res.redirect("../../list");
});

app.get('/login', (req, res) => {
    res.render('articles/index.ejs')
});

app.post('/dashboard', (req, res) => {
    const user = posts.find(user => user.username === req.body.username)
    if (user) {
        const isMatch = user.password === req.body.password;
        if (isMatch) {
            res.redirect('dashboard/list');

        } else {
            res.status(404).send("PASSWORD NOT VALID")
        }
    } else {
        res.status(404).send("USERNAME NOT VALID")
    }
})

app.get('/', (req, res) => {
    res.render('landing/index.ejs')
});

app.get('/home', (req, res) => {
    res.render('landing/index.ejs')
});

app.get('/playnow', (req, res) => {
    res.render('game/index.ejs')
});

app.get('/datauser', (req, res) => {
    res.status(200).json(posts)
});

app.get("/dashboard/create", (req, res) => {
    res.render("dashboard/create.ejs")
});

app.listen(port, () => console.log(`example app listening at ${port}`))
