'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    // return queryInterface.bulkInsert('user_game_history', [{
    //   score: 100,
    //   lastPlayed: new Date(),
    //   userId: 15,
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // }]);

    // return queryInterface.bulkInsert('user_game_biodata', [{
    //   firstName: 'asdfasg',
    //   lastName: 'asasgda',
    //   UserId: 20,
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // }]);

    return queryInterface.bulkInsert('history', [{
      userId: 22,
      score: 100,
      lastPlayed: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('history', null, {});
  }
};
